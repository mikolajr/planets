package org.rydzewski.planets;

/**
 * org.rydzewski.planets.Planet
 * @author Mikolaj Rydzewski
 */
public class Planet {
	
	/**
	 * x coordinate
	 */
	double x;
	
	/**
	 * y coordinate
	 */
	double y;
	
	/**
	 * weight 
	 */
	double m;
	
	Force a = new Force();
	Force gravity = new Force();
	
	public Planet(double x, double y, double m) {
		super();
		this.x = x;
		this.y = y;
		this.m = m;
	}
	
	double distance(double x, double y) {
		double dx = x - this.x;
		double dy = y - this.y;
		
		double distance = Math.sqrt(dx*dx + dy*dy);
		return distance;
	}

	double distance(Planet p) {
		return distance(p.x, p.y);
	}

	@Override
	public String toString() {
		return String.format("x %f, y %f m %f\n", x, y, m);
	}

}
