package org.rydzewski.planets;

/**
 * org.rydzewski.planets.Force vector
 * @author Mikolaj Rydzewski
 */
public class Force implements Cloneable {

	public Force() {
	}

	public Force(double x, double y) {
		this.x = x;
		this.y = y;
	}

	double x;
	double y;
	
	Force add(Force f) {
		x += f.x;
		y += f.y;
		return this;
	}
	
	Force substract(Force f) {
		x -= f.x;
		y -= f.y;
		return this;
	}
	
	Force multiply(double s) {
		x *= s;
		y *= s;
		return this;
	}
	
	Force divide(double s) {
		x /= s;
		y /= s;
		return this;
	}
	
	double length() {
		return Math.sqrt(x*x + y*y);
	}

	@Override
	protected Force clone()  {
		Force f = new Force();
		f.x = this.x;
		f.y = this.y;
		return f;
	}
}
