package org.rydzewski.planets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

import javax.swing.JPanel;


public class Space extends JPanel implements Runnable {
	
	static final int GRAVITY_FACTOR = 2000;
	static final int ACCEL_FACTOR = 50;
	static final float WEIGHT_FACTOR = 150000f;
	
	Matrix matrix;
	Thread t;
	volatile boolean running = false;
	volatile boolean showAccel = true;
	volatile boolean showGravity = true;
	
	Space(Matrix matrix) {
		super();
		setDoubleBuffered(true);
		this.matrix = matrix;
		t = new Thread(this);
		t.setDaemon(true);
		t.start();
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(600, 600);
	}

	@Override
	protected void paintComponent (Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		g.clearRect(0, 0, getWidth(), getHeight());
		
		synchronized (matrix) {
			for(Planet planet : matrix.getPlanets()) {
				drawPlanet(planet, g2);
			}
		}
	}

	void drawPlanet(Planet planet, Graphics2D g) {
		int r = (int)(planet.m / WEIGHT_FACTOR);
		if (r < 1) r=1;
		
		g.setColor(Color.BLACK);
		g.drawOval((int)planet.x-r, (int)planet.y-r , 2*r, 2*r);
		
		if (showAccel) {
			g.setColor(Color.RED);
			drawVector((Graphics2D)g, planet.x, planet.y, planet.a, ACCEL_FACTOR);
		}
		
		if (showGravity) {
			g.setColor(Color.BLUE);
			drawVector(g, planet.x, planet.y, planet.gravity, GRAVITY_FACTOR);
		}
	}
	
   private void drawVector(Graphics2D g, double x, double y, Force v, int accelFactor) {
	   AffineTransform at = AffineTransform.getTranslateInstance(x, y);
	   at.rotate(v.x, v.y);
	   Shape shape = at.createTransformedShape(createVector((int)(v.length() * accelFactor)));
       g.draw(shape);
	}

   private Path2D.Double createVector(int length) {
        int barb = 5;
        double angle = Math.toRadians(20);
        double x = length - barb*Math.cos(angle);
        double y = barb*Math.sin(angle);
        
        Path2D.Double path = new Path2D.Double();
        path.moveTo(0, 0);
        path.lineTo(length, 0);
        path.lineTo(x, y);
        path.lineTo(x, -y);
        path.lineTo(length, 0);

        return path;
    }


	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			    break;
			}
			
			matrix.calculateFieldForces();

			if (running) {
				matrix.movePlanets();
				invalidate();
				repaint();
			}
		}
	}
	
	void start() {
		running = true;
	}
	
	void stop() {
		running = false;
	}
	
	boolean isRunning() {
		return running;
	}

	void toggleGravity() {
		showAccel = !showAccel;
	}

}
