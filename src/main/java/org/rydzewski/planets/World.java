package org.rydzewski.planets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;


public class World extends JFrame {
	
	protected static final double DEFAULT_WEIGHT = 1000000;
	Matrix matrix;
	Space space;
	Planet newPlanet;
	Planet edit;
	
	JTextField xfield;
	JTextField yfield;
	JTextField mfield;

	public World() {
		super();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());

		matrix = createMatrix();
		space = new Space(matrix);
		space.addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseDragged(MouseEvent e) {
				if (space.running || newPlanet==null) return;
				int x = e.getX();
				int y = e.getY();
				
				Graphics g = space.getGraphics();
				g.setColor(space.getBackground());
				g.drawLine((int)newPlanet.x, (int)newPlanet.y, (int)(newPlanet.x + newPlanet.a.x), (int)(newPlanet.y + newPlanet.a.y));
				
				newPlanet.a.x = x - newPlanet.x;
				newPlanet.a.y = y - newPlanet.y;
				g.setColor(Color.GREEN);
				g.drawLine((int)newPlanet.x, (int)newPlanet.y, (int)(newPlanet.x + newPlanet.a.x), (int)(newPlanet.y + newPlanet.a.y));
				g.dispose();
			}

		});
		space.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				if (space.running) {
					space.stop();
				} else {
					if (SwingUtilities.isLeftMouseButton(e)) {
						int x = e.getX();
						int y = e.getY();
						double m = DEFAULT_WEIGHT;
						newPlanet = new Planet(x, y, m);
						
						Graphics g = space.getGraphics();
						space.drawPlanet(newPlanet, (Graphics2D) g);
						g.dispose();
					} else if(SwingUtilities.isRightMouseButton(e)) {
						Planet planet = matrix.findPlanet(e.getX(), e.getY());
						if (planet != null) {
							edit = planet;
							mfield.setText(Double.valueOf(planet.m).toString());
						}
					}
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (space.running || newPlanet == null) return;
				newPlanet.a.divide(Space.ACCEL_FACTOR);
				matrix.addPlanet(newPlanet);
				edit = newPlanet;
				newPlanet = null;
				mfield.setText(Double.valueOf(edit.m).toString());
			}
		});
		add(space, BorderLayout.CENTER);
		space.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		add(makeConfigPane(), BorderLayout.EAST);
		
		pack();
		
	}

	private JPanel makeConfigPane() {
		JPanel panel2 = new JPanel();
		panel2.setBorder(BorderFactory.createTitledBorder("tytul"));
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
		
		JPanel panel3 = new JPanel();
		panel2.add(panel3);
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));
		panel3.add(new JLabel("X"));
		panel3.add(Box.createRigidArea(new Dimension(5, 0)));
		xfield = new JTextField();
		xfield.setColumns(20);
		xfield.setMaximumSize( xfield.getPreferredSize() ); 
		panel3.add(xfield);
		panel3.setMaximumSize(panel3.getPreferredSize());
		
		panel3 = new JPanel();
		panel2.add(panel3);
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));
		panel3.setBorder(BorderFactory.createTitledBorder(""));
		panel3.add(new JLabel("Y"));
		yfield = new JTextField();
		yfield.setColumns(20);
		yfield.setMaximumSize( yfield.getPreferredSize() ); 
		panel3.add(yfield);
		
		panel3 = new JPanel();
		panel2.add(panel3);
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));
		panel3.add(new JLabel("M"));
		mfield = new JTextField();
		mfield.setColumns(20);
		mfield.setMaximumSize( mfield.getPreferredSize() ); 
		panel3.add(mfield);
		mfield.addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent e) {
				try {
					Double d = Double.valueOf(mfield.getText());
					edit.m = d;
					
					Graphics g = space.getGraphics();
					space.drawPlanet(edit, (Graphics2D) g);
					g.dispose();
				}
				catch (Exception ex) {
				}
			}
		});

		
		final JCheckBox showAccel = new JCheckBox("Show accel force");
		showAccel.setSelected(space.showAccel);
		showAccel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				space.showAccel = showAccel.isSelected();
				space.invalidate();
				space.repaint();
			}
		});
		panel2.add(showAccel);
		
		final JCheckBox showGravity = new JCheckBox("Show gravity force");
		showGravity.setSelected(space.showGravity);
		showGravity.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				space.showGravity = showGravity.isSelected();
				space.invalidate();
				space.repaint();
			}
		});
		panel2.add(showGravity);
		
		
		
		final JButton startStop = new JButton("Start");
		startStop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (space.isRunning()) {
					space.stop();
					startStop.setText("Start");
				} else {
					space.start();
					startStop.setText("Stop");
				}
			}
		});
		panel2.add(startStop);
		
		final JButton restart = new JButton("Restart");
		restart.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				space.stop();
				int answer = JOptionPane.showConfirmDialog(World.this, "Are you sure to start again?", null, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null);
				if (answer != JOptionPane.YES_OPTION)
					space.start();
				else {
					startStop.setText("Start");
					matrix.clear();
					space.invalidate();
					space.repaint();
				}
			}
		});
		panel2.add(restart);
		
		

		return panel2 ;
	}

	private Matrix createMatrix() {
		Matrix matrix = new Matrix();
		return matrix;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new World().setVisible(true);
			}
		});
	}

}
