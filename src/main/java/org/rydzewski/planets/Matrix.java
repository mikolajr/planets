package org.rydzewski.planets;

import java.util.ArrayList;
import java.util.List;



/**
 * org.rydzewski.planets.Matrix
 * @author Mikolaj Rydzewski
 */
public class Matrix {
	
	private static final double G = 0.00005;
	
	private List<Planet> planets = new ArrayList<Planet>();
	
	synchronized void calculateFieldForces() {
		// for every planet sum gravity forces to all other planets
		for(Planet planet : planets) {
			Force gravityForce = planet.gravity;
			gravityForce.x = gravityForce.y = 0;
			
			for(Planet neighbour : planets) {
				if (!planet.equals(neighbour)) {
					
					double distance = planet.distance(neighbour);
					double gravity = G * planet.m * neighbour.m / (distance * distance);
					
					// gravity is the length of a vector, now we should point it
					// from planet to neighbour
					Force vector = new Force(neighbour.x - planet.x, neighbour.y - planet.y);
					vector.divide(vector.length());
					vector.multiply(gravity);
					
					gravityForce.add(vector);
				}
			}
			gravityForce.divide(planet.m);
		}
	}

	synchronized void movePlanets() {
		for(Planet planet : planets) {
			planet.a.add(planet.gravity);
			planet.x += planet.a.x;
			planet.y += planet.a.y;
		}
		
		// any collisions?
		Collision c = null;
		while ((c = findCollision()) != null) {
			Planet winner, looser;
			
			if (c.p1.m > c.p2.m) {
				winner = c.p1;
				looser = c.p2;
			} else {
				winner = c.p2;
				looser = c.p1;
			}

			// let's create directed vectors with length of 1
			Force winnerA = winner.a.clone();
			Force looserA = looser.a.clone();
			
			winnerA.divide(winnerA.length());
			looserA.divide(looserA.length());
			
			double newLength = (winner.m * winner.a.length() + looser.m * looser.a.length()) / (winner.m + looser.m);
			
			winner.m += looser.m;
			winner.a = winnerA.add(looserA).multiply(newLength);
			
			planets.remove(looser);
		}
	}
	
	Collision findCollision() {
		for(Planet planet : planets) {
			for(Planet neighbour : planets) {
				if (!planet.equals(neighbour)) {
					if (planet.distance(neighbour) <= 2)
						return new Collision(planet, neighbour);
				}
			}
		}
		return null;
	}
	
	class Collision {
		Planet p1;
		public Collision(Planet p1, Planet p2) {
			this.p1 = p1;
			this.p2 = p2;
		}
		Planet p2;
	}

	List<Planet> getPlanets() {
		return planets;
	}

	synchronized boolean addPlanet(Planet e) {
		return planets.add(e);
	}

	synchronized Planet findPlanet(int x, int y) {
		for(Planet planet : planets) {
			if (planet.distance(x, y) <= 2)
				return planet;
		}
		
		return null;
	}

	synchronized void clear() {
		planets.clear();
	}
	
}
